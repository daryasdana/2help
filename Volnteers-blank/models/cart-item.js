class CartItem {
  constructor(quantity, productPrice, requestTitle, sum,name) {
    this.quantity = quantity;
    this.productPrice = productPrice;
    this.requestTitle = requestTitle;
    this.sum = sum;
    this.name=name;
  }
}

export default CartItem;
