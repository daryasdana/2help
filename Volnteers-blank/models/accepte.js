import moment from 'moment';

class Accept {
  constructor(id, ownerId,items, totalAmount, date, description, Username, Useremail) {
    this.id = id;
    this.ownerId = ownerId;
    this.items = items;
    this.totalAmount = totalAmount;
    this.date = date;
    this.description = description;
    this.name = Username;
    this.name = Useremail;

  }

  get readableDate() {
  
    return moment(this.date).format('MMMM Do YYYY, hh:mm');
  }
}

export default Accept;
