class Missingpeople {
    constructor(id, ownerId, FullName, description, AgeAtDisappearance,MissingSince,imageUrl,lat,lng) {
      this.id = id;
      this.ownerId = ownerId;
      this.FullName = FullName;
      this.description = description;
      this.AgeAtDisappearance = AgeAtDisappearance;
      this.MissingSince = MissingSince;
      this.imageUrl = imageUrl;
      this.lat = lat;
      this.lng = lng;
    }
  }
  
  export default Missingpeople;