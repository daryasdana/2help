import React, {Component,useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  View,
  UIManager,
  StyleSheet,
  LayoutAnimation,
  Button,
  Text,
  FlatList
} from 'react-native';

import Colors from '../../constants/Colors';
import * as ordersActions from '../../store/actions/accepted';
import CartItem from '../../components/shop/CartItem';
import * as cartActions from '../../store/actions/cart';

LayoutAnimation.spring();

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

import{ CreditCardInput } from 'react-native-credit-card-input';
//import { FlatList } from 'react-native-gesture-handler';

const Mayment = props => 
{
 
  const [isLoading, setIsLoading] = useState(false);
  const cartTotalAmount = useSelector(state => state.cart.totalAmount);
  const cartItems = useSelector(state => {
    const transformedCartItems = [];
    for (const key in state.cart.items) {
      transformedCartItems.push({
        productId: key,
        requestTitle: state.cart.items[key].requestTitle,
        productPrice: state.cart.items[key].productPrice,
        quantity: state.cart.items[key].quantity,
        sum: state.cart.items[key].sum,
        
       
      });
    }
    
    return transformedCartItems.sort((a, b) =>
      a.productId > b.productId ? 1 : -1
    );
  });
      const dispatch = useDispatch();
      const sendOrderHandler = async () => {
        setIsLoading(true);
        await dispatch(ordersActions.addOrder(cartItems, cartTotalAmount));
        alert("Payment Successful");
        setIsLoading(false);
        props.navigation.goBack();
        props.navigation.navigate('accepted')
    };

    
{
    return (
      <View style={styles.container}>
        <CreditCardInput
          autoFocus
          requireName={true}
          requireCVC={true}
          requirePostalCode={true}
          validColor="black"
          invalidColor="red"
          placeholderColor="darkgray"
          labelStyle={{color: 'black', fontSize: 12}}
          inputStyle={{color: 'black', fontSize: 16}}

        />
          <Text style={styles.amountt}>
         Total Donations £{Math.round(cartTotalAmount.toFixed(2) * 100) / 100}
          </Text>
          <View style={styles.amount}>
        <Button
          
          style={styles.amount}
          title="Pay Now"
          disabled={cartItems.length === 0}
          onPress={sendOrderHandler}

        />
        </View>
        <FlatList
        data={cartItems}
        keyExtractor={item => item.productId}
        renderItem={itemData => (
          <CartItem
            quantity={itemData.item.quantity}
            title={itemData.item.requestTitle}
            amount={itemData.item.sum}
            deletable
            onRemove={() => {
              dispatch(cartActions.removeFromCart(itemData.item.productId));
            }}
          />
        )}
      />

      </View>
    );
  

};
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 5,
      backgroundColor: 'white',
    },
    amountt:{
        color: Colors.primary,
        marginTop:20,
        marginLeft:120,
        fontSize: 20
        
    },
    amount:{
      width: '50%',
      alignSelf: 'center',
      marginVertical: 30,
      backgroundColor:'pink',
      borderRadius:30
      
      
  },

  });
  export default Mayment;
  
