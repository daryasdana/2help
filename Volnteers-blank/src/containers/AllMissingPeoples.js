/*
This is the page where I am showing all the missing peopes 
*/

import React, {useState, useEffect, useCallback} from 'react';
import {
    View,
    Text,
    FlatList,
    Button,
    ActivityIndicator,
    StyleSheet,
    Alert
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import MissingPeoples from '../../components/shop/MissingPeoples';
import * as missingpeopleActions from '../../store/actions/missingpeople';
import Colors from '../../constants/Colors';

const AllMissingPeoples = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const missingpeople = useSelector(state => state.missingpeople.availableMissingpeople);
 /* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 
    
    const dispatch = useDispatch();

    const loadMissingpeople = useCallback(async () => {
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(missingpeopleActions.fetchMissingpeople());
        } catch (err) {
            setError(err.message);
        }
        setIsLoading(false);
    }, [dispatch, setIsLoading, setError]);
  /*
  this willFocus is for refreshing the page without leaving the page 
  */

    useEffect(() => {
        const willFocusSub = props.navigation.addListener(
            'willFocus',
            loadMissingpeople
        );

        return () => {
            willFocusSub.remove();
        };
    }, [loadMissingpeople]);

    useEffect(() => {
        loadMissingpeople();
    }, [dispatch, loadMissingpeople]);

/*
the selectItemHandler function will call when the user 
touch the missing peoples and it will take them to that missing peoples detail page 
we are passing the id and the FullName of the missing peoples so it know which person to show 
*/

    const selectItemHandler = (id, FullName) => {
        props.navigation.navigate('MissingPeopleDetailPage', {
            missingpeopleId: id,
            missingpeopleFullName: FullName
        });
    };

    if (error) {
        return (
     /*
      if the data is not loading is will show them this error message 
      with with a button where they can click and it will reload the data
      */
            <View style={styles.centered}>
                <Text>An error occurred!</Text>
                <Button
                    title="Try again"
                    onPress={loadMissingpeople}
                    color={Colors.primary}
                />
            </View>
        );
    }
  /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */
    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
        );
    }
//If No missing peopel is added it will show this message 
    if (!isLoading && missingpeople.length === 0) {
        return (
            <View style={styles.centered}>
                <Text>No missing people was added </Text>
            </View>
        );
    }

    return (
    //this is for rendering the data and extracting them by their unique id
     //I am using a FlatList because I don't know how long the list might be
        <FlatList
          
            data={missingpeople}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <MissingPeoples
                    image={itemData.item.imageUrl}
                    title={itemData.item.FullName}
                    price={itemData.item.AgeAtDisappearance}
                    quantityNeeded={itemData.item.MissingSince}
                    onSelect={() => {
                        selectItemHandler(itemData.item.id, itemData.item.FullName);
                    }}
                >
                    <Button
                    // this button will call the selectItemHandler which will take them to the detail page
                        color={Colors.primary}
                        style={styles.btn}
                        title="View Detailss"
                        onPress={() => {
                            selectItemHandler(itemData.item.id, itemData.item.FullName);
                        }}
                    />
                 <Button
                 color={Colors.primary}
                 title={"Did you saw this person ?"}
                 onPress={() => {
                 Alert.alert(
                 "Did you saw this person ?",
                 'If you saw this person please email Support@2help.co.uk',
                    [{text: 'OK'}]
                      );
                     }}
    
      />
                </MissingPeoples>
            )}
        />
    );
};

AllMissingPeoples.navigationOptions = navData => {
    return {
         //adding headerTitle
        headerTitle: 'All Missing Peoples',

    };
};

const styles = StyleSheet.create({
    centered: {flex: 1, justifyContent: 'center', alignItems: 'center'},
   
 
});

export default AllMissingPeoples;
