/*
This is the page where I am showing all
the Progress Update where the emergency team uploaded
*/

import React, {useState, useEffect, useCallback} from 'react';
import {
    View,
    Text,
    FlatList,
    Button,
    ActivityIndicator,
    StyleSheet,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import ProductItem from '../../components/shop/ProductItem';
import * as ProgressUpdateActions from '../../store/actions/ProgressUpdate';
import Colors from '../../constants/Colors';

const ProgressUpdate = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const dispatch = useDispatch();
    const userProgressUpdate = useSelector(state => state.ProgressUpdate.userProgressUpdates);
 /* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 
 
    const loadProducts = useCallback(async () => {
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(ProgressUpdateActions.fetchProgressUpdate());
        } catch (err) {
            setError(err.message);
        }
        setIsLoading(false);
    }, [dispatch, setIsLoading, setError]);
  /*
  this willFocus is for refreshing the page without leaving the page 
  */
    useEffect(() => {
        const willFocusSub = props.navigation.addListener(
            'willFocus',
            loadProducts
        );

        return () => {
            willFocusSub.remove();
        };
    }, [loadProducts]);

    useEffect(() => {
        loadProducts();
    }, [dispatch, loadProducts]);
/*
the selectItemHandler function will call when the user 
touch the progress  and it will take them to that Progress Update Detaile page 
we are passing the id and the title of the progress so it know which reqest to show 
*/

    const selectItemHandler = (id, title) => {
        props.navigation.navigate('ProgressUpdateDetailScreen', {
            productId: id,
            requestTitle: title
        });
    };
        /*
      if the data is not loading is will show them this error message 
      with with a button where they can click and it will reload the data
      */

    if (error) {
        return (
            <View style={styles.centered}>
                <Text>An error occurred!</Text>
                <Button
                    title="Try again"
                    onPress={loadProducts}
                    color={Colors.primary}
                />
            </View>
        );
    }
  /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */
    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
        );
    }
//If No progress is submitted it will show this message 
    if (!isLoading && userProgressUpdate.length ===0) {
        return (
            <View style={styles.centered}>
                <Text>No progress update was uploaded yet!</Text>
            </View>
        );
    }

    /*
     this is for rendering the data and extracting them by their unique id
     I am using a FlatList because I don't know how long the list might be
    */
    return (
        <FlatList
            style={styles.p}
            data={userProgressUpdate}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <ProductItem

                    image={itemData.item.imageUrl}
                    title={itemData.item.title}
                    price={itemData.item.price}
                    onSelect={() => {
                        selectItemHandler(itemData.item.id, itemData.item.title);
                    }}
                >
                    
                    <Button
                      // this button will call the selectItemHandler which will take them to the detail page
                         color={Colors.primary}
                        style={styles.btn}
                        title="View Details"
                        onPress={() => {
                            selectItemHandler(itemData.item.id, itemData.item.title);
                        }}
                    />
                </ProductItem>
            )}
        />
    );
};

ProgressUpdate.navigationOptions = props => {
 //adding headerTitle
    return {
        headerTitle: 'Progress Updadate',
        
    };
};

const styles = StyleSheet.create({
    centered: {flex: 1, justifyContent: 'center', alignItems: 'center'},

    pr: {
        padding: 140,
        margin: 100
    },
    btn: {
        marginTop: 100,
        padding: 10,
        textAlign:'center',
        
    }
});

export default ProgressUpdate;
