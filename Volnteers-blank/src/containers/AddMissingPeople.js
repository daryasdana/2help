/*
this the page where the user fill all the information of the
missing peoples
*/


import React, {useEffect, useCallback, useReducer, useState} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    Platform,
    Alert,
    KeyboardAvoidingView,
    Text
} from 'react-native';
import {Button} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import * as missingpeopleActions from '../../store/actions/missingpeople';
import Input from '../../components/UI/Input';
import ImgPicker from '../../components/ImagePicher';
import LocationPicker from '../../components/LocationPicker';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        };
        const updatedValidates = {
            ...state.inputValidates,
            [action.input]: action.isValid
        };
        let updatedFormIsValid = true;
        for (const key in updatedValidates) {
            updatedFormIsValid = updatedFormIsValid && updatedValidates[key];
        }
        return {
            formIsValid: updatedFormIsValid,
            inputValidates: updatedValidates,
            inputValues: updatedValues
        };
    }
    return state;
};

const AddMissingpeopleScreen = props => {


    const [selectedImage, setSelectedImage] = useState('');
    const [selectedLocation, setSelectedLocation] = useState('');
    const dispatch = useDispatch();
/*
this is where I am doing the user's input validation
if the fields are empty the form is not valid and this will show them a error
*/

    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            FullName: '',
            imageUrl: '',
            description: '',
            AgeAtDisappearance: '',
            MissingSince: '',
        },
        inputValidates: {
            FullName: false,
            description: false,
            AgeAtDisappearance: false,
            MissingSince: false,
        },
        formIsValid: false
    });

    let imageTakenHandler;
    imageTakenHandler = imagePath => {
        setSelectedImage(imagePath);
    };

    let locationPickedHandler = useCallback(location => {
        //console.log(location);
        setSelectedLocation(location);
    }, []);


    let validForm = () => {
        if (!formState.formIsValid) {
            Alert.alert('Wrong input!', 'Please check the errors in the form.', [
                {text: 'OK'}
            ]);
            return false
        }
        return true
    };

    /*
    this is where I am sending the user's input to the store action and it will
    save them on the data base is the form is valid. 
    if it is not valid it will retun to the form.
    */
    const addMissingpeopleHandler = useCallback(() => {
        if (!validForm()) {
            return
        }
        dispatch(
            missingpeopleActions.createMissingpeople(
                formState.inputValues.FullName,
                formState.inputValues.description,
                +formState.inputValues.AgeAtDisappearance,
                formState.inputValues.MissingSince,
                selectedImage,
                selectedLocation
            )
        );
        //if the form is valid and saved in the data back it will return to the home page
        props.navigation.goBack();
        props.navigation.navigate('AllMissingPeoples');
       
    });

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => {
            dispatchFormState({
                type: FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier
            });
        },
        [dispatchFormState]
    );
/*
this is where the use fill the form
I am using the KeyboardAvoidingView do that the user can alway see what they are typing
*/
    return (
        <KeyboardAvoidingView
            style={{flex: 1}}
            behavior="padding"
            keyboardVerticalOffset={100}
        >
            <ScrollView>
                <View style={styles.form}>
                    <Input
                        id="FullName"
                        label="Full Name"
                        errorText="Please enter a valid FullName!"
                        keyboardType="default"
                        autoCapitalize="sentences"
                        autoCorrect
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="AgeAtDisappearance"
                        label="Age At Disappearance"
                        errorText="Please enter a valid AgeAtDisappearance!"
                        keyboardType="decimal-pad"
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="description"
                        label="Description"
                        errorText="Please enter a valid description!"
                        keyboardType="default"
                        returnKeyType="next"
                        autoCapitalize="sentences"
                        autoCorrect
                        multiline
                        numberOfLines={3}
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="MissingSince"
                        label="Missing Since"
                        errorText="Please enter the date!"
                        keyboardType="numeric"
                        onInputChange={inputChangeHandler}
                    />

                    <ImgPicker onImageTaken={imageTakenHandler}/>
                    <Text>Please Locate where was seen last time</Text>
                    <LocationPicker navigation={props.navigation} onLocationPicked={locationPickedHandler}/>
                    <Button onPress={addMissingpeopleHandler}>Add Missing People</Button>

                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    );
};


const styles = StyleSheet.create({
    form: {
        margin: 20,
    }
});

export default AddMissingpeopleScreen;
