import React, { useEffect, useState } from 'react';
import {
  View,
  FlatList,
  Text,
  ActivityIndicator,
  StyleSheet,
 
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import OrderItem from '../../components/shop/OrderItem';
import * as ordersActions from '../../store/actions/accepted';
import Colors from '../../constants/Colors';
const OrdersScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const userAcceptedRequests = useSelector(state => state.accepted.accepted);
  const dispatch = useDispatch();
/* the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 
  
  /*
this useEffect function is saying the the data is Loading then fetch the data from the database
 and after it is teched changing the isLoading to false
  */

  useEffect(() => {
    setIsLoading(true);
    //console.log(orders);
    dispatch(ordersActions.fetchAccepted()).then(() => {
      setIsLoading(false);
    });
  }, [dispatch]);

  /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }
/*
this will check if the user accepted any jobs.
of not it will show this message
*/
  if (!isLoading && userAcceptedRequests.length === 0) {
    return (
        <View style={styles.centered}>
            <Text>You didn't accept any requests! </Text>
        </View>
    );
}

  return (
    //this is for rendering the data and extracting them by their unique id
    <FlatList
      data={userAcceptedRequests}
      keyExtractor={item => item.id}
    renderItem={itemData => (
        <OrderItem
          amount={itemData.item.totalAmount}
          date={itemData.item.readableDate}
          items={itemData.item.items}
          name={itemData.item.name}
          description={itemData.item.description}
        />
      )}
    />
  );
};


const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default OrdersScreen;
