/*
This is the cart page when the user accept a job or a donation
they will come to this page to see what they accept and confirm it
*/

import React, {useEffect, useCallback, useReducer, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  Button,
  StyleSheet,
  ActivityIndicator,
 
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import Colors from '../../constants/Colors';
import CartItem from '../../components/shop/CartItem';
import Card from '../../components/UI/Card';
import * as cartActions from '../../store/actions/cart';
import * as ordersActions from '../../store/actions/accepted';

const CartScreen = props => {
  /* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 
  const [isLoading, setIsLoading] = useState(false);
  const cartTotalAmount = useSelector(state => state.cart.totalAmount);
  const cartItems = useSelector(state => {
    const transformedCartItems = [];
    for (const key in state.cart.items) {
      transformedCartItems.push({
        productId: key,
        requestTitle: state.cart.items[key].requestTitle,
        productPrice: state.cart.items[key].productPrice,
        quantity: state.cart.items[key].quantity,
        sum: state.cart.items[key].sum,
       
      });
    }
    
    return transformedCartItems.sort((a, b) =>
      a.productId > b.productId ? 1 : -1
    );
  });
  const dispatch = useDispatch();

  const sendOrderHandler = async () => {
    if(cartTotalAmount>0){
      props.navigation.navigate('Payment')

    }
    else{
      setIsLoading(true);
      await dispatch(ordersActions.addOrder(cartItems, cartTotalAmount));
      alert("Accepted Successfully");
      setIsLoading(false);
    }
   
  };

  return (
    <View style={styles.screen}>
      <Card style={styles.summary}>
        <Text style={styles.summaryText}>
          Total Donations:{' '}
          <Text style={styles.amount}>
            £{Math.round(cartTotalAmount.toFixed(2) * 100) / 100}
          </Text>
        </Text>
        {isLoading ? (
          <ActivityIndicator size="small" color={Colors.primary} />
        ) : (
          <Button
          
            color={Colors.accent}
            title="Accept Now"
            disabled={cartItems.length === 0}
            onPress={sendOrderHandler}
  
          />
        )}
      </Card>
      {/* 
    
    this is for rendering the data and extracting them by their unique id
     I am using a FlatList because I don't know how long the list might be 
     */}
      <FlatList
        data={cartItems}
        keyExtractor={item => item.productId}
        renderItem={itemData => (
          <CartItem
            quantity={itemData.item.quantity}
            title={itemData.item.requestTitle}
            amount={itemData.item.sum}
            deletable
            
            onRemove={() => {
              dispatch(cartActions.removeFromCart(itemData.item.productId));
            }}
          />
        )}
      />
    </View>
    
  );
  
};

CartScreen.navigationOptions = {
  headerTitle: 'You Accepted'
};

const styles = StyleSheet.create({
  screen: {
    margin: 20
  },
  summary: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
    padding: 10
  },
  summaryText: {
    
    fontSize: 18
  },
  amount: {
    color: Colors.primary
  }
});

export default CartScreen;
