
/*
This is the page where I show All the request that the emergency team requested
*/

import React, { useState, useEffect, useCallback } from 'react';
import {
  View,
  Text,
  FlatList,
  Button,
  Platform,
  ActivityIndicator,
  StyleSheet
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';
import ProductItem from '../../components/shop/ProductItem';
import * as cartActions from '../../store/actions/cart';
import * as productsActions from '../../store/actions/requests';
import Colors from '../../constants/Colors';

const NewRequest = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const requests = useSelector(state => state.requests.availableProducts);
  const dispatch = useDispatch();
/* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 
  const loadRequests = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);
    try {
      await dispatch(productsActions.fetchProducts());
    } catch (err) {
      setError(err.message);
    }
    // setIsLoading(false);
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);
  /*
  this willFocus is for refreshing the page without leaving the page 
  */
  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      'willFocus',
      loadRequests
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadRequests]);

  useEffect(() => {
    setIsLoading(true);
    loadRequests().then(()=>{
      setIsLoading(false);
    });
  }, [dispatch, loadRequests]);
/*
the selectRequestHandler function will call when the user 
touch the request and it will take them to that request detail page 
we are passing the id and the title of the request so it know which reqest to show 
*/

  const selectRequestHandler = (id, title) => {
    props.navigation.navigate('RequestsDetailScreen', {
      productId: id,
      requestTitle: title
    });
  };




  if (error) {
    return (
      /*
      if the data is not loading is will show them this error message 
      with with a button where they can click and it will reload the data
      */
      <View style={styles.centered}>
        <Text>An error occurred!</Text>
        <Button
          title="Try again"
          onPress={loadRequests}
          color={Colors.primary}
        />
      </View>
    );
  }
  /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */
  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }
//If No Request is submitted it will show this message 
  if (!isLoading && requests.length === 0) {
    return (
      <View style={styles.centered}>
        <Text>No request found!</Text>
      </View>
    );
  }
    /*
     this is for rendering the data and extracting them by their unique id
     I am using a FlatList because I don't know how long the list might be
     */
  return ( 
    <FlatList
    onRefresh={loadRequests}
    refreshing={isRefreshing}
      data={requests}
      keyExtractor={item => item.id}
      renderItem={itemData => (
        <ProductItem
          image={itemData.item.imageUrl}
          title={itemData.item.title}
          price={itemData.item.price}
          onSelect={() => {
            selectRequestHandler(itemData.item.id, itemData.item.title,itemData.item.imageUrl);
            
          }}
        >
          <Button
          // this button will call the selectRequestHandler which will take them to the detail page
            color={Colors.primary}
            title="View Details"
            onPress={() => {
              selectRequestHandler(itemData.item.id, itemData.item.title,itemData.item.imageUrl);
            }}
          />
          <Button
          //this button will add the request to the cart where the user can confirm the requests 
            color={Colors.primary}
            title="Accept"
            onPress={() => {
              props.navigation.navigate('Cart');
              dispatch(cartActions.addToCart(itemData.item,itemData.item.imageUrl));
            }}
          />
        </ProductItem>
      )}
    />
  );
};

NewRequest.navigationOptions = navData => {
  return {
    //adding headerTitle
    headerTitle: 'All Requests',
  
//this is to add the Header Button when you press it will take you to the Cart page
// where you can confirm the requests
    headerRight: () =>
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Cart"
          iconName={Platform.OS === 'android' ? 'md-checkmark' : 'ios-checkmark'}
          onPress={() => {
            navData.navigation.navigate('Cart');
          }}
        />
      </HeaderButtons>
    
  };
};

const styles = StyleSheet.create({
  centered: { flex: 1, justifyContent: 'center', alignItems: 'center' }
});

export default NewRequest;

