
/*
This is the page where I am showing the user's profile 
*/

import React, {useState, useEffect, useCallback} from 'react';
import {
    View,
    Text,
    FlatList,
    Button,
    ActivityIndicator,
    StyleSheet,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Profile from '../../components/shop/Profile';
import * as userInfoActions from '../../store/actions/userInfo';
import Colors from '../../constants/Colors';

const ProfilePage = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const dispatch = useDispatch();
    const userUserInfo = useSelector(state => state.userInfo.userUserInfo);


    const loadProducts = useCallback(async () => {
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(userInfoActions.fetchUserInfo());
        } catch (err) {
            setError(err.message);
        }
        setIsLoading(false);
    }, [dispatch, setIsLoading, setError]);

    useEffect(() => {
        const willFocusSub = props.navigation.addListener(
            'willFocus',
            loadProducts
        );

        return () => {
            willFocusSub.remove();
        };
    }, [loadProducts]);

    useEffect(() => {
        loadProducts();
    }, [dispatch, loadProducts]);

    /*
when the user press the profile this function will call
and it will navigate it to the users profile detail page where 
the user can read all the feedback they got 
    */
    const selectItemHandler = (id, namee) => {
        props.navigation.navigate('ProfileDetail', {
            userInfoId: id,
            userInfoName: namee
        });
    };
    /*
    this function will call whe the user press edit profile 
    it will take them to the edit profile page 
    I am passing the id so they can edit their profile 
    */
    const updateItemHandler = (id) => {
        props.navigation.navigate('EditProfilePage', {
            userInfoId: id,
        });
    };

       /*
      if the data is not loading is will show them this error message 
      with with a button where they can click and it will reload the data
      */

    if (error) {
        return (
            <View style={styles.centered}>
                <Text>An error occurred!</Text>
                <Button
                    title="Try again"
                    onPress={loadProducts}
                    color={Colors.primary}
                />
            </View>
        );
    }
  /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */
    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
        );
    }

     //this is for rendering the data and extracting them by their unique id
     //I am using a FlatList because I don't know how long the list might be
    return (
        <FlatList
            style={styles.p}
            data={userUserInfo}
            
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <Profile

                    image={itemData.item.imageUrl}
                    title={itemData.item.namee}
                    price={itemData.item.email}
                    phonenumber={itemData.item.phonenumber}
                    adress={itemData.item.adress}
                    quantityNeeded={itemData.item.dateofbirth}
                    onSelect={() => {
                        selectItemHandler(itemData.item.id, itemData.item.namee);
                    }}
                >

                        <Button
                         color={Colors.primary}
                        style={styles.btn}
                        title="Edit"
                        onPress={() => {
                            updateItemHandler(itemData.item.id);
                        }}
                            />
                    <Button
                         color={Colors.primary}
                        style={styles.btn}
                        title=">"
                        onPress={() => {
                            selectItemHandler(itemData.item.id, itemData.item.namee);
                        }}
                    />

                </Profile>
            )}
        />
    );
};

ProfilePage.navigationOptions = navData => {
    return {
        headerTitle: 'My Profile',

    };
};

const styles = StyleSheet.create({
    centered: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    p: {
         //padding: 1,

    },
    pr: {
        padding: 140,
        margin: 100
    },
    btn: {
        marginTop: 100,
        padding: 10,
    }
});

export default ProfilePage;