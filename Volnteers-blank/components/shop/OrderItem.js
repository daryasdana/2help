import React, { useState } from 'react';
import { View, Text, Button, StyleSheet,Image,Alert } from 'react-native';
import CartItem from './CartItem';
import Colors from '../../constants/Colors';
import Card from '../UI/Card';

const OrderItem = props => {
  const [showDetails, setShowDetails] = useState(false);

  return (
    <Card style={styles.orderItem}>
      <View style={styles.summary}>
        <Text style={styles.totalAmount}>£{props.amount}</Text>
        <Text style={styles.date}>{props.date}</Text>
        
      </View>
      {/* <Text style={styles.name}>{props.name}</Text> */}
      <Image style={styles.image} source={{ uri: props.image }} />
      <Button
        color={Colors.primary}
        title={showDetails ? 'Hide Details' : 'Show Details'}
        onPress={() => {
          setShowDetails(prevState => !prevState);
        }}
      />
      {showDetails && (
        <View style={styles.detailItems}>
          {props.items.map(cartItem => (
            <CartItem
              key={cartItem.productId}
              quantity={cartItem.quantity}
              amount={cartItem.sum}
              title={cartItem.requestTitle}
              description={cartItem.description}

            />
          ))}
      <Button
        color={Colors.primary}
        title={"Can't do this work ?"}
        onPress={() => {
          
          Alert.alert(
            "Can't do this work ?",
            'in order to cancel please email Support@2help.co.uk',
            [{text: 'OK'}]
        );
        }}
    
      />

        </View>
      )}
    </Card>
  );
};

const styles = StyleSheet.create({
  orderItem: {
    margin: 20,
    padding: 10,
    alignItems: 'center'
  },
  summary: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginBottom: 15
  },
  totalAmount: {
    
    fontSize: 16
  },
  date: {
    fontSize: 16,
    
    color: '#888'
  },
  detailItems: {
    width: '100%'
  },
  name: {
    
  }
});

export default OrderItem;
