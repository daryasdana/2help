import { articles_url, _api_key, country_code,q,sortBy } from './rest_consfig';


export async function getArticles(category='health') {

    try {
        let articles = await fetch(`${articles_url}?${q}&sortBy=${sortBy}`, {
            headers: {
                'X-API-KEY': _api_key
            },
           
        });

        let result = await articles.json();
        articles = null;

        return result.articles;
    }
    catch(error) {
        throw error;
    }
}