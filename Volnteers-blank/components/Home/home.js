/*
This is the Home page where I display the News and add the button to navigate to the other pages 

*/
import React, { Component } from 'react';
import TabScreen from './TabScreen';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    Image,
} from 'react-native';

export default class Home extends React.Component{
  static navigationOptions = {
    headerShown: false,
  }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View >
            <View style={styles.container}>
            <Image 
                 style={styles.image}
                  source={require('../../assets/logo.png')} />
            </View>
            < View style={styles.boxx} >
          <View style={styles.news} >
              <Text style={styles.newstext}>News  </Text>
              {/* This is where I am showing the news  */}
              <TabScreen/>
             
          </View>
          </View>
          
      <View style={styles.barr}>
      <ScrollView horizontal={true}showsHorizontalScrollIndicator={false} >
        <TouchableOpacity 
                onPress={() => navigate('NewRequests')}>
                  <Image
                  style={styles.imagee}
                   source={require('../../assets/search.png')} />
               </TouchableOpacity>
                <TouchableOpacity  
                 onPress={() => navigate('accepted')}>
                   <Image
                   style={styles.imageee}
                    source={require('../../assets/Accepted.png')} />
                </TouchableOpacity> 
                <TouchableOpacity  
                 onPress={() => navigate('ProgressUpdate')}>
                   <Image
                   style={styles.imageee}
                    source={require('../../assets/progressupdat.png')} />
                </TouchableOpacity>
               
                <TouchableOpacity
                 onPress={() => navigate('AddMissingPeople')}>
                   <Image
                   style={styles.imageee}
                    source={require('../../assets/MissingPeople.png')}/>
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={() => navigate('AllMissingPeoples')}>
                  <Image
                  style={styles.imageee}
                   source={require('../../assets/AllMissingpeoples.png')} />
               </TouchableOpacity>
            </ScrollView>
             </View>
            </View>   
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    justifyContent: "center",
         alignItems: "center",
    },
    btnTxt:{
        padding:10,
         width: "90%",
        },
        image:{
            marginBottom:-90,
            width: 90,
            height: 90,
            resizeMode: 'contain',
           },
           barr: {
             flexDirection:'row',
           },
        imagee:{
           width: 170,
           height: 203,
           resizeMode: 'contain',
            marginTop:10,
          },
          imageee:{
           width: 170,
           height: 190,
           resizeMode: 'contain',
           marginTop:13,
          },

        news:{
        margin:10,
        marginTop:90,
        borderRadius:10,
        width: 350,
        height: 400,
        width:"95%",
        shadowColor:'black',
        shadowOffset:{width:0, height:2},
        shadowRadius:6,
        shadowOpacity:0.26,
        elevation:8,
        backgroundColor:'white',
    },
          newstext:{
          color:"#b1acb7",
          fontSize:30,
          textAlign:"center",
           
        },
        gridItem: {
             flex: 1,
            margin: 10,
             
          },
          newstextt: {
            fontSize:20,
         },
         boxx: {
            overflow:'hidden',
          
         },

});










