import React, { Component } from 'react';
import { Container } from 'native-base';
import Tab1 from './tabs/tab1';

export default class TabsExample extends Component {
  render() {
    return (
      <Container>
       <Tab1 />
      </Container>
    );
  }
}