import { combineReducers } from 'redux';
import navReducer from '../reducers/navigation';
import sessionReducer from '../reducers/session';
import chatReducer from '../reducers/chat';
import productsReducer from '../store/reducers/requests';
import userInfoReducer from '../store/reducers/userInfo';
import missingpeopleReducer from '../store/reducers/missingpeople';
import cartReducer from '../store/reducers/cart';
import ordersReducer from '../store/reducers/accepted';
import ProgressUpdateReducer from "../store/reducers/ProgressUpdate";
import reviewReducer from "../store/reducers/review";

//adding the roodReducers 
export default combineReducers({
  nav: navReducer,
  sessionReducer,
  chatReducer,
  requests: productsReducer,
  missingpeople:missingpeopleReducer,
  cart:cartReducer,
  accepted: ordersReducer,
  ProgressUpdate: ProgressUpdateReducer,
  review: reviewReducer,
  userInfo:userInfoReducer
});
