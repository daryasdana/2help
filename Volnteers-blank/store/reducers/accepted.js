import { ADD_REQUEST, SET_ACCEPT } from '../actions/accepted';
import Accept from '../../models/accepte';
//we are defing the initialState
const initialState = {
  accepted: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ACCEPT:
      return {
        accepted: action.accepted
      };
    case ADD_REQUEST:
      const newAccept = new Accept(
        action.acceptedData.id,
        action.acceptedData.ownerId,
        action.acceptedData.items,
        action.acceptedData.amount,
        action.acceptedData.date,
        action.acceptedData.description,
        action.acceptedData.Username,
        action.acceptedData.Useremail
        
      );
      return {
        ...state,
        accepted: state.accepted.concat(newAccept)
      };
  }

  return state;
};
