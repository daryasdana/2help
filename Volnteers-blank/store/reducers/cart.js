import { ADD_TO_CART, REMOVE_FROM_CART } from '../actions/cart';
import { ADD_REQUEST } from '../actions/accepted';
import CartItem from '../../models/cart-item';
import { DELETE_REQUEST } from '../actions/requests';
//we are defing the initialState
const initialState = {
  items: {},
  totalAmount: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:

      const addedRequest = action.request;
      const prodPrice = addedRequest.price;
      const prodTitle = addedRequest.title;

      let updatedOrNewCartItem;

      if (state.items[addedRequest.id]) {
        // already have the item in the cart
        updatedOrNewCartItem = new CartItem(
          state.items[addedRequest.id].quantity + 1,
          prodPrice,
          prodTitle,
          state.items[addedRequest.id].sum + prodPrice
        );
      } else {
        //if we arready did not added we add a new one 
        updatedOrNewCartItem = new CartItem(1, prodPrice, prodTitle, prodPrice);
      }
      return {
        //coyping the state
        ...state,
        items: { ...state.items, [addedRequest.id]: updatedOrNewCartItem },
        totalAmount: state.totalAmount + prodPrice
      };
    case REMOVE_FROM_CART:
      const selectedCartItem = state.items[action.pid];
      const currentQty = selectedCartItem.quantity;
      let updatedCartItems;
      if (currentQty > 1) {
        // need to reduce it, not erase it
        const updatedCartItem = new CartItem(
          selectedCartItem.quantity - 1,
          selectedCartItem.productPrice,
          selectedCartItem.requestTitle,
          selectedCartItem.sum - selectedCartItem.productPrice
        );
        updatedCartItems = { ...state.items, [action.pid]: updatedCartItem };
      } else {
        updatedCartItems = { ...state.items };
        //If there is only 1 delete it 
        delete updatedCartItems[action.pid];
      }
      return {
        ...state,
        items: updatedCartItems,
        totalAmount: state.totalAmount - selectedCartItem.productPrice
      };
      //to reset the cart after accepting the requests
    case ADD_REQUEST:
      return initialState;
    case DELETE_REQUEST:
      if (!state.items[action.pid]) {
        return state;
      }
      const updatedItems = { ...state.items };
      const itemTotal = state.items[action.pid].sum;
      delete updatedItems[action.pid];
      return {
        ...state,
        items: updatedItems,
        totalAmount: state.totalAmount - itemTotal
      };
  }

  return state;
};
