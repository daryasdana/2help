
import {SET_ProgressUpdate} from '../actions/ProgressUpdate';
//we are defing the initialState
  const initialState = {
    availableProgressUpdates: [],
    userProgressUpdates: []
  };
  
  export default (state = initialState, action) => {
    switch (action.type) {
      case SET_ProgressUpdate:
        return {
          availableProgressUpdates: action.ProgressUpdates,
          userProgressUpdates: action.userProgressUpdates
        };
      }
    return state;
  };
  
  