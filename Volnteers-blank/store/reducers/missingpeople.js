
import {
    CREATE_MISSINGPEOPLE,
    SET_MISSINGPEOPLE
  } from '../actions/missingpeople';

  import Missingpeople from '../../models/missingpeople';
  //we are defing the initialState
  const initialState = {
    availableMissingpeople: [],
    userMissingpeople: [],
  
  };
  
  export default (state = initialState, action) => {
    switch (action.type) {
      case SET_MISSINGPEOPLE:
        return {
          availableMissingpeople: action.missingpeople,
          userMissingpeople: action.userMissingpeople,

        };
        
      case CREATE_MISSINGPEOPLE:
        const newMissingpeople = new Missingpeople(
          action.missingpeopleData.id,
          action.missingpeopleData.ownerId,
          action.missingpeopleData.FullName,
          action.missingpeopleData.imageUrl,
          action.missingpeopleData.description,
          action.missingpeopleData.AgeAtDisappearance,
          action.missingpeopleData.image,
          action.missingpeopleData.MissingSince,
          action.missingpeopleData.image,
          action.missingpeopleData.lat,
          action.missingpeopleData.lng,
          
        );
        return {
          ...state,
          availableMissingpeople: state.availableMissingpeople.concat(newMissingpeople),
          userMissingpeople: state.userMissingpeople.concat(newMissingpeople),

          
        };
        
    }
    return state;
  };
  
  
  