import { CREATE_REVIEW, SET_REVIEW } from "../actions/review";

import Review from "../../models/missingpeople";
//we are defing the initialState
const initialState = {
  reviews: [],
  userReviews: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_REVIEW:
      return {
        reviews: action.reviews,
        userReviews: action.userReviews,

      };
  }
  return state;
};
