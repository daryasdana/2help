import {
  SET_Requests
} from '../actions/requests';

//we are defing the initialState
const initialState = {
  availableProducts: [],
  userProducts: []
};

// this fuction is difating the state with the initialState
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_Requests:
      return {
        availableProducts: action.requests,
        userProducts: action.userProducts
      }
    }
  return state;

  }

