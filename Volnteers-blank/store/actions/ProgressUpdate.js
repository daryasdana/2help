
export const SET_ProgressUpdate = 'SET_ProgressUpdate';
import * as firebase from "firebase";
import ProgressUpdate from '../../models/ProgressUpdate';

export const fetchProgressUpdate = () => {
    return async dispatch => {
        // any async code you want!
        const userId = firebase.auth().currentUser.uid;
        const name = firebase.auth().currentUser.displayName
        try {
            const response = await fetch(
                'https://help-6faf1.firebaseio.com/ProgressUpdate.json'
            );

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }

            const resData = await response.json();
            const ProgressUpdates = [];

            for (const key in resData) {
                ProgressUpdates.push(
                    new ProgressUpdate(
                        key,
                        resData[key].ownerId,
                        resData[key].title,
                        resData[key].description,
                        resData[key].price,
                        resData[key].imageUrl,
                        resData[key].name,
                    )
                );
            }

            dispatch({
                type: SET_ProgressUpdate,
                ProgressUpdates: ProgressUpdates,
                userProgressUpdates:ProgressUpdates
                
            });
            
        } catch (err) {
            throw err;
        }
    };
};
