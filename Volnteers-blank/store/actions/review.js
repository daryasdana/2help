//this page is responsible for adding the form the the database or update or delete them
import Review from "../../models/review";
export const SET_REVIEW = "SET_REVIEW";
import * as firebase from "firebase";

export const fetchReview = (userInfoId) => {
  return async (dispatch) => {
    const userId = firebase.auth().currentUser.uid;
    try {
      const response = await fetch(
        "https://help-6faf1.firebaseio.com/Review.json"
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const resData = await response.json();
      const reviews = [];

      for (const key in resData) {
        reviews.push(
          new Review(
            key,
            resData[key].ownerId,
            resData[key].review,
            resData[key].name

              )
        );
      }

      dispatch({
        type: SET_REVIEW,
        reviews: reviews,
        userReviews: reviews.filter((prod) => prod.ownerId === userInfoId),
      });
      //console.log(requests)
    } catch (err) {
      // send to custom analytics server
      throw err;
    }
  };
};

