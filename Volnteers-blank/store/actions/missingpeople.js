//this page is responsible for adding the form the the database or update or delete them 
import Missingpeople from '../../models/missingpeople';
export const CREATE_MISSINGPEOPLE = 'CREATE_MISSINGPEOPLE';
export const SET_MISSINGPEOPLE = 'SET_MISSINGPEOPLE';
import ENV from '../../env';
import * as firebase from "firebase";

export const fetchMissingpeople = () => {
    return async dispatch => {
        // any async code you want!
        const userId = firebase.auth().currentUser.uid;
        try {
            const response = await fetch(
                'https://help-6faf1.firebaseio.com/missingpeople.json'
            );

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }

            const resData = await response.json();
            const missingpeople = [];
            //console.log(resData);

            for (const key in resData) {
                missingpeople.push(
                    new Missingpeople(
                        key,
                        resData[key].ownerId,
                        resData[key].FullName,
                        resData[key].description,
                        resData[key].AgeAtDisappearance,
                        resData[key].MissingSince,
                        resData[key].imageUrl,
                        resData[key].lat,
                        resData[key].lng,
                    )
                );
            }

            dispatch({
                type: SET_MISSINGPEOPLE,
                missingpeople: missingpeople,
                userMissingpeople: missingpeople.filter(prod => prod.ownerId === userId)
            });
            //console.log(requests)
        } catch (err) {
            // send to custom analytics server
            throw err;
        }
    };
};



const getImageFromFirebase = async (uri) => {
    const imageName = Math.round(+new Date()/1000) + "." + uri.substr(uri.lastIndexOf('.') +1);
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase.storage().ref().child("images/" + imageName);
    const snapshot = await ref.put(blob);
    const imgURL = await snapshot.ref.getDownloadURL();
    return imgURL
};

export const createMissingpeople = (FullName, description, AgeAtDisappearance, MissingSince, imageUri,location) => {
    return async dispatch => {
        const userId = firebase.auth().currentUser.uid;
        const imageUrl = await getImageFromFirebase(imageUri);
        const lat = location.lat;
        const lng = location.lng;


        const payload = JSON.stringify({
            FullName,
            description,
            AgeAtDisappearance,
            ownerId: userId,
            MissingSince,
            imageUrl,
            lat,
            lng

        });

        const response = await fetch(
            'https://help-6faf1.firebaseio.com/missingpeople.json',
            

            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: payload,
               

            }
        );

        const resData = await response.json();
        //console.log(resData);

        dispatch({
            type: CREATE_MISSINGPEOPLE,
            missingpeopleData: {
                id: resData.name,
                FullName,
                description,
                AgeAtDisappearance,
                ownerId: userId,
                MissingSince,
                imageUrl,
                lat,
                lng

            }
        });
    };
};

