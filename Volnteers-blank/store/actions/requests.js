//this page is responsible for adding the form the the database or update or delete them 
import request from '../../models/requests';
export const SET_Requests = 'SET_Requests';
import * as firebase from "firebase";

export const fetchProducts = () => {
    return async dispatch => {
        const userId = firebase.auth().currentUser.uid;
        try {
            const response = await fetch(
                'https://help-6faf1.firebaseio.com/requests.json'
            );

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }
            const resData = await response.json();
            const requests = [];
            for (const key in resData) {
                requests.push(
                    new request(
                        key,
                        resData[key].ownerId,
                        resData[key].title,
                        resData[key].description,
                        resData[key].price,
                        resData[key].quantityNeeded,
                        resData[key].imageUrl,
                        resData[key].lat,
                        resData[key].lng,
                    )
                );
            }

            dispatch({
                type: SET_Requests,
                requests: requests.filter(prod => prod.quantityNeeded >=1),
                userProducts: requests.filter(prod => prod.ownerId === userId)
            });
            
        } catch (err) {
            // send to custom analytics server
            throw err;
        }
    };
};


