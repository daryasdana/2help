import Accept from '../../models/accepte';
export const ADD_REQUEST = 'ADD_REQUEST';
export const SET_ACCEPT = 'SET_ORDERS';
import * as firebase from "firebase";
 

export const fetchAccepted = () => {
  return async dispatch => {

   const userId=firebase.auth().currentUser.uid;

    try {
      const response = await fetch(
        `https://help-6faf1.firebaseio.com/AcceptedRequests.json`
      );

      if (!response.ok) {
        throw new Error('Something went wrong!');
      }

      const resData = await response.json();
      const loadedOrders = [];
      const accepted = [];

      for (const key in resData) {
        loadedOrders.push(
          new Accept(
            key,
            resData[key].ownerId,
            resData[key].cartItems,
            resData[key].totalAmount,
            new Date(resData[key].date),
            resData[key].description,
            resData[key].Username,
            resData[key].Useremail,
          )
        );
        
      }
      dispatch({
         type: SET_ACCEPT,
          accepted: loadedOrders.filter(prod => prod.ownerId === userId)
         });
        
    } catch (err) {
      throw err;
    }
  };
};


export const addOrder = (cartItems, totalAmount,description) => {
 
  return async (dispatch,getState) => {
    const userId=firebase.auth().currentUser.uid;
    const Username=firebase.auth().currentUser.displayName;
    const Useremail=firebase.auth().currentUser.email;

    const date = new Date();
    const response = await fetch(
      `https://help-6faf1.firebaseio.com/AcceptedRequests.json`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          cartItems,
          totalAmount,
          date: date.toISOString(),
          description,
          ownerId: userId,
          Username,
          Useremail,
          
        })
      }
    );

    if (!response.ok) {
      throw new Error('Something went wrong!');
    }

    const resData = await response.json();

    dispatch({
      type: ADD_REQUEST,
      acceptedData: {
        id: resData.name,
        items: cartItems,
        amount: totalAmount,
        date: date,
        description:description,
        ownerId: userId,
        Username,
        Useremail
      }
    });
  };
};
